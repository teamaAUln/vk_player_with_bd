﻿namespace VK_AudioPlayer
{
    partial class EditMusic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_Title = new System.Windows.Forms.Label();
            this.textBox_Title = new System.Windows.Forms.TextBox();
            this.label_Artist = new System.Windows.Forms.Label();
            this.textBox_Artist = new System.Windows.Forms.TextBox();
            this.label_genre = new System.Windows.Forms.Label();
            this.button_Change = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.comboBox_genre = new System.Windows.Forms.ComboBox();
            this.label_text = new System.Windows.Forms.Label();
            this.textBox_text = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_Title
            // 
            this.label_Title.AutoSize = true;
            this.label_Title.Location = new System.Drawing.Point(12, 55);
            this.label_Title.Name = "label_Title";
            this.label_Title.Size = new System.Drawing.Size(125, 13);
            this.label_Title.TabIndex = 0;
            this.label_Title.Text = "Название аудиозаписи";
            // 
            // textBox_Title
            // 
            this.textBox_Title.Location = new System.Drawing.Point(143, 52);
            this.textBox_Title.Name = "textBox_Title";
            this.textBox_Title.Size = new System.Drawing.Size(167, 20);
            this.textBox_Title.TabIndex = 1;
            // 
            // label_Artist
            // 
            this.label_Artist.AutoSize = true;
            this.label_Artist.Location = new System.Drawing.Point(12, 18);
            this.label_Artist.Name = "label_Artist";
            this.label_Artist.Size = new System.Drawing.Size(74, 13);
            this.label_Artist.TabIndex = 2;
            this.label_Artist.Text = "Исполнитель";
            // 
            // textBox_Artist
            // 
            this.textBox_Artist.Location = new System.Drawing.Point(143, 15);
            this.textBox_Artist.Name = "textBox_Artist";
            this.textBox_Artist.Size = new System.Drawing.Size(167, 20);
            this.textBox_Artist.TabIndex = 3;
            // 
            // label_genre
            // 
            this.label_genre.AutoSize = true;
            this.label_genre.Location = new System.Drawing.Point(12, 91);
            this.label_genre.Name = "label_genre";
            this.label_genre.Size = new System.Drawing.Size(36, 13);
            this.label_genre.TabIndex = 4;
            this.label_genre.Text = "Жанр";
            // 
            // button_Change
            // 
            this.button_Change.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Change.Location = new System.Drawing.Point(12, 239);
            this.button_Change.Name = "button_Change";
            this.button_Change.Size = new System.Drawing.Size(148, 23);
            this.button_Change.TabIndex = 6;
            this.button_Change.Text = "Применить";
            this.button_Change.UseVisualStyleBackColor = true;
            this.button_Change.Click += new System.EventHandler(this.button_Change_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Cancel.Location = new System.Drawing.Point(166, 239);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(148, 23);
            this.button_Cancel.TabIndex = 7;
            this.button_Cancel.Text = "Отменить";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // comboBox_genre
            // 
            this.comboBox_genre.FormattingEnabled = true;
            this.comboBox_genre.Items.AddRange(new object[] {
            "Rock",
            "Pop",
            "Rap & Hip-Hop",
            "Easy Listening",
            "Dance & House",
            "nstrumental",
            "Metal",
            "Dubstep",
            "Jazz & Blues",
            "Drum & Bass",
            "Trance",
            "Chanson",
            "Ethnic",
            "Acoustic & Vocal",
            "Reggae",
            "Classical",
            "Indie Pop",
            "Other",
            "Speech",
            "Alternative",
            "Electropop & Disco"});
            this.comboBox_genre.Location = new System.Drawing.Point(143, 88);
            this.comboBox_genre.Name = "comboBox_genre";
            this.comboBox_genre.Size = new System.Drawing.Size(167, 21);
            this.comboBox_genre.TabIndex = 8;
            // 
            // label_text
            // 
            this.label_text.AutoSize = true;
            this.label_text.Location = new System.Drawing.Point(12, 121);
            this.label_text.Name = "label_text";
            this.label_text.Size = new System.Drawing.Size(37, 13);
            this.label_text.TabIndex = 9;
            this.label_text.Text = "Текст";
            // 
            // textBox_text
            // 
            this.textBox_text.Location = new System.Drawing.Point(15, 137);
            this.textBox_text.Multiline = true;
            this.textBox_text.Name = "textBox_text";
            this.textBox_text.Size = new System.Drawing.Size(295, 86);
            this.textBox_text.TabIndex = 10;
            // 
            // EditMusic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(322, 274);
            this.Controls.Add(this.textBox_text);
            this.Controls.Add(this.label_text);
            this.Controls.Add(this.comboBox_genre);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Change);
            this.Controls.Add(this.label_genre);
            this.Controls.Add(this.textBox_Artist);
            this.Controls.Add(this.label_Artist);
            this.Controls.Add(this.textBox_Title);
            this.Controls.Add(this.label_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditMusic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование Аудиозаписи";
            this.Load += new System.EventHandler(this.EditMusic_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Title;
        private System.Windows.Forms.TextBox textBox_Title;
        private System.Windows.Forms.Label label_Artist;
        private System.Windows.Forms.TextBox textBox_Artist;
        private System.Windows.Forms.Label label_genre;
        private System.Windows.Forms.Button button_Change;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.ComboBox comboBox_genre;
        private System.Windows.Forms.Label label_text;
        private System.Windows.Forms.TextBox textBox_text;
    }
}