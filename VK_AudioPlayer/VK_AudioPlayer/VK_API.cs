﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Collections;
using System.IO;
using System.Xml.XPath;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Net;
using System.Windows.Forms;
//using Un4seen.Bass;


namespace VK_AudioPlayer
{
    class VK_API
    {
        public class Audio
        {
            public int aid { get; set; }
            public int owned_id { get; set; }
            public string artist { get; set; }
            public string title { get; set; }
            public int duration { get; set; }
            public string url { get; set; }
            public string lyrics_id { get; set; }
            public int genre { get; set; }

            //public Audio(int _aid, int _owned_id, string _artist, string _title, int _duration, string _url, string _lyrics_id, int _genre) 
            //{
            //    this.aid = _aid;
            //    this.owned_id = _owned_id;
            //    this.artist = _artist;
            //    this.title = _title;
            //    this.duration = _duration;
            //    this.url = _url;
            //    this.lyrics_id = _lyrics_id;
            //    this.genre = _genre;
            //}
        }

        public List<Audio> AudioSearch(string _queryText, int _searh_own) 
        {
            WebClient Client = new WebClient();
            List<Audio> ListAudio = new List<Audio>();

            // Подготовка к парсингу аудиозаписей
            // Settings1.Default.id - id пользователя
            // Settings1.Default.token - 

            using (Stream strm = Client.OpenRead("https://api.vk.com/method/audio.search.xml?q="+ _queryText +"&auto_complete=1&lyrics=0&performer_only=0&sort=2&search_own=1&offset=0&count=100&access_token="+ Settings1.Default.token))
            {
                StreamReader sr = new StreamReader(strm);
                FileStream f = new FileStream("Audio.xml", FileMode.Create);
                StreamWriter sw = new StreamWriter(f);
                sw.WriteLine(sr.ReadToEnd());
                sw.Close();
                f.Close();
                sr.Close();
            }

            // Переносим инфу из Audio.xml сперва в листксемейле - а дальше вытаскиваем нужное нам.
            XmlDocument doc = new XmlDocument();
            XmlTextReader reader = new XmlTextReader("Audio.xml");
            doc.Load(reader);
            XmlNodeList elemList = doc.GetElementsByTagName("audio");

            ListAudio = fillingAudioList(elemList, ListAudio);
            reader.Close();
            return ListAudio;
        }

        public void EditAudio(string audio_id, string artist, string title, string text, int genre_id)
        {
            WebClient Client = new WebClient();
            List<Audio> ListAudio = new List<Audio>();

            using (Stream strm = Client.OpenRead("https://api.vk.com/method/audio.edit.xml?owner_id=" + Settings1.Default.id + "&audio_id=" + audio_id + "&artist=" + artist + "&title=" + title + "&text=" + text + "&genre_id=" + genre_id + "&access_token=" + Settings1.Default.token))
            {
                StreamReader sr = new StreamReader(strm);
                FileStream f = new FileStream("AudioEdit.xml", FileMode.Create);
                StreamWriter sw = new StreamWriter(f);
                sw.WriteLine(sr.ReadToEnd());
                sw.Close();
                f.Close();
                sr.Close();
            }

            XmlDocument doc = new XmlDocument();
            XmlTextReader reader = new XmlTextReader("AudioEdit.xml");
            doc.Load(reader);
            XmlNodeList elemList = doc.GetElementsByTagName("response");
            reader.Close();
        }

        public List<Audio> AudioGet()
        {
            WebClient Client = new WebClient();
            List<Audio> ListAudio = new List<Audio>();

            // Подготовка к парсингу аудиозаписей
            // Settings1.Default.id - id пользователя
            // Settings1.Default.token - 
            
            using (Stream strm = Client.OpenRead("https://api.vk.com/method/audio.get.xml?owned_id=" + Settings1.Default.id + "&need_user=0&access_token=" + Settings1.Default.token))
            {
                StreamReader sr = new StreamReader(strm);
                FileStream f = new FileStream("Audio.xml", FileMode.Create);
                StreamWriter sw = new StreamWriter(f);
                sw.WriteLine(sr.ReadToEnd());
                sw.Close();
                f.Close();
                sr.Close();
            }

            // Переносим инфу из Audio.xml сперва в листксемейле - а дальше вытаскиваем нужное нам.
            XmlDocument doc = new XmlDocument();
            XmlTextReader reader = new XmlTextReader("Audio.xml");
            doc.Load(reader);
            XmlNodeList elemList = doc.GetElementsByTagName("audio");

            ListAudio = fillingAudioList(elemList, ListAudio);
            reader.Close();
            return ListAudio;
        }

        //парсинг XML файла и добавление в лист списка песен
        public List<Audio> fillingAudioList(XmlNodeList a, List<Audio> b)
        {
            for (int i = 0; i < a.Count; i++)
            {
                Audio audio = new Audio();

                for (int j = 0; j < a[i].ChildNodes.Count; j++)
                {

                    if (a[i].ChildNodes[j].Name.ToString() == "aid")
                    {
                        audio.aid = Convert.ToInt32(a[i].ChildNodes.Item(j).InnerText.ToString());
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "owned_id")
                    {
                        audio.owned_id = Convert.ToInt32(a[i].ChildNodes.Item(j).InnerText.ToString());
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "artist")
                    {
                        audio.artist = a[i].ChildNodes.Item(j).InnerText.ToString();
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "title")
                    {
                        audio.title = a[i].ChildNodes.Item(j).InnerText.ToString();
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "duration")
                    {
                        audio.duration = Convert.ToInt32(a[i].ChildNodes.Item(j).InnerText.ToString());
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "url")
                    {
                        audio.url = a[i].ChildNodes.Item(j).InnerText.ToString();
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "lyrics_id")
                    {
                        audio.lyrics_id = a[i].ChildNodes.Item(j).InnerText.ToString();
                    }
                    if (a[i].ChildNodes[j].Name.ToString() == "genre")
                    {
                        audio.genre = Convert.ToInt32(a[i].ChildNodes.Item(j).InnerText.ToString());
                    }
                }
                b.Add(audio);
                
            }
            return b;
        }
    }        
}


//https://api.vk.com/mthod/audio.get.xml?owned_id=98657549&need_user=0&access_token=88a2756cffef12126167be35fdec2f2e164ae212c9d852e3f343d169a887acc52e517d5a134d2fb6c350b