﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VK_AudioPlayer
{
    public partial class EditMusic : Form
    {
        public EditMusic()
        {
            InitializeComponent();
        }
        public Main fm1 = new Main();
        string temp = String.Empty;
        int indexAudio;

        private void EditMusic_Load(object sender, EventArgs e)
        {
            //temp = fm1.AudioBox.SelectedItem.ToString();
            //indexAudio = fm1.AudioBox.SelectedIndex;
            textBox_Artist.Text = fm1.AudioListSearchAudioArtist(indexAudio);
            textBox_Title.Text = fm1.AudioListSearchAudioTitle(indexAudio);
            GenreId(fm1.AudioListSearchAudioGenre(indexAudio));
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void GenreId(int index)
        {
            switch (index)
            {
                case 1:
                    {
                        comboBox_genre.SelectedIndex = 0;
                    } break;
                case 2:
                    {
                        comboBox_genre.SelectedIndex = 1;
                    } break;
                case 3:
                    {
                        comboBox_genre.SelectedIndex = 2;
                    } break;
                case 4:
                    {
                        comboBox_genre.SelectedIndex = 3;
                    } break;
                case 5:
                    {
                        comboBox_genre.SelectedIndex = 4;
                    } break;
                case 6:
                    {
                        comboBox_genre.SelectedIndex = 5;
                    } break;
                case 7:
                    {
                        comboBox_genre.SelectedIndex = 6;
                    } break;
                case 8:
                    {
                        comboBox_genre.SelectedIndex = 7;
                    } break;
                case 9:
                    {
                        comboBox_genre.SelectedIndex = 8;
                    } break;
                case 10:
                    {
                        comboBox_genre.SelectedIndex = 9;
                    } break;
                case 11:
                    {
                        comboBox_genre.SelectedIndex = 10;
                    } break;
                case 12:
                    {
                        comboBox_genre.SelectedIndex = 11;
                    } break;
                case 13:
                    {
                        comboBox_genre.SelectedIndex = 12;
                    } break;
                case 14:
                    {
                        comboBox_genre.SelectedIndex = 13;
                    } break;
                case 15:
                    {
                        comboBox_genre.SelectedIndex = 14;
                    } break;
                case 16:
                    {
                        comboBox_genre.SelectedIndex = 15;
                    } break;
                case 17:
                    {
                        comboBox_genre.SelectedIndex = 16;
                    } break;
                case 18:
                    {
                        comboBox_genre.SelectedIndex = 17;
                    } break;
                case 19:
                    {
                        comboBox_genre.SelectedIndex = 18;
                    } break;
                case 20:
                    {
                        comboBox_genre.SelectedIndex = 19;
                    } break;
                case 21:
                    {
                        comboBox_genre.SelectedIndex = 20;
                    } break;
                case 22:
                    {
                        comboBox_genre.SelectedIndex = 21;
                    } break;
            }

        }

        private void button_Change_Click(object sender, EventArgs e)
        {
            VK_API vk_api = new VK_API();
            vk_api.EditAudio(fm1.AudioListSearchAudioid(indexAudio).ToString(), textBox_Artist.Text, textBox_Title.Text, textBox_text.Text, comboBox_genre.SelectedIndex + 1);
            this.Close();
        }
    }
}
