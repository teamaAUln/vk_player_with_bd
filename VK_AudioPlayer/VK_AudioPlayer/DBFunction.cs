﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VK_AudioPlayer
{
    class DBFunction
    {       
        //Добавление музыки в БД
        public void MusicAdd(int ownedID, string artist, string title, int duration, string url,
            int lyricsID, int genre)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Music music = new Music()
                {
                    OwnedID = Convert.ToInt32(ownedID),
                    Artist = artist,
                    Title = title,
                    Duration = Convert.ToInt32(duration),
                    URL = url,
                    LyricsID = Convert.ToInt32(lyricsID),
                    Genre = Convert.ToInt32(genre),
                };
                if (context.Music.Any(ur => ur.URL == music.URL) == false)
                {
                    context.Music.Add(music);
                }
                try
                {
                    context.SaveChanges();
                    //MessageBox.Show("Новая композиция добавлена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Редактирование музыки в БД
        public void MusicUpdate(int ownedID, string artist, string title, int duration, string url,
            int lyricsID, int genre, int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Music music = context.Music.Where(id => id.MusicID == choicecell).First();
                music.OwnedID = Convert.ToInt32(ownedID);
                music.Artist = artist;
                music.Title = title;
                music.Duration = Convert.ToInt32(duration);
                music.URL = url;
                music.LyricsID = Convert.ToInt32(lyricsID);
                music.Genre = Convert.ToInt32(genre);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Композиция изменена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Удаление музыки в БД
        public void MusicDelete(int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Music music = context.Music.Where(id => id.MusicID == choicecell).First();
                context.Music.Remove(music);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Композиция удалена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Добавление пользователя в БД
        public void UserAdd(int vkID, string name)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                User user = new User()
                {
                   VkID = Convert.ToInt32(vkID),
                   Name = name,
                };
                context.User.Add(user);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Новый пользователь добавлен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Редактирование пользователя в БД
        public void UserUpdate(int vkID, string name, int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                User user = context.User.Where(id => id.UserID == choicecell).First();
                user.VkID = Convert.ToInt32(vkID);
                user.Name = name;
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Пользователь изменен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Удаление пользователя в БД
        public void UserDelete(int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                List<MusicList> musicList = context.MusicList.Where(id => id.UserID == choicecell).ToList();
                User user = context.User.Where(id => id.UserID == choicecell).First();
                context.User.Remove(user);
                context.MusicList.RemoveRange(musicList);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Пользователь удален!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Добавление лога в БД
        public void LogAdd(int userID, DateTime dataTime, string note)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Log log = new Log()
                {
                    UserID = Convert.ToInt32(userID),
                    Date = Convert.ToDateTime(dataTime),
                    Note = note,
                };
                context.Log.Add(log);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Новый лог добавлен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Редактирование лога в БД
        public void LogUpdate(int userID, DateTime dataTime, string note, int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Log log = context.Log.Where(id => id.LogID == choicecell).First();
                log.UserID = Convert.ToInt32(userID);
                log.Date = Convert.ToDateTime(dataTime);
                log.Note = note;
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Лог изменен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Очистка списка песен
        public void DeletaAllMusic()
        {
            Music Mc = new Music();

            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                context.Music.RemoveRange(context.Music);
                context.SaveChanges();
            }
        }
        //Удаление лога в БД
        public void LogDelete(int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Log log = context.Log.Where(id => id.LogID == choicecell).First();
                context.Log.Remove(log);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Лог удален!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Добавление альбома в БД
        public void MusicListAdd(int userID, string name, int amount)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                MusicList musicList = new MusicList()
                {
                    UserID = Convert.ToInt32(userID),
                    Name = name,
                    Amount = Convert.ToInt32(amount),
                };
                context.MusicList.Add(musicList);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Новый список добавлен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        // Возвращаем песню по айдишнику песни
        public Music ExMusic(int SongID)
        {
            Music Mc = new Music();
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Mc = context.Music.Where(id => id.MusicID == SongID).First();
            }
            return Mc;
        }

        // Возвращаем песню по URL
        public Music ExMusicbyURL(string SongURL)
        {
            Music Mc = new Music();
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Mc = context.Music.Where(ur => ur.URL == SongURL).First();
            }
            return Mc;
        }

        //Возвращение песен по плейлисту
        public MusicList[] ExMusicbyList(int ListID)
        {
            MusicList Mcl = new MusicList();
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                return context.MusicList.Where(id => id.MusicListID == ListID).ToArray();
            }
        }
        

        //Редактирование альбома в БД
        public void MusicListUpdate(int userID, string name, int amount, int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                MusicList musicList = context.MusicList.Where(id => id.MusicListID == choicecell).First();
                musicList.UserID = Convert.ToInt32(userID);
                musicList.Name = name;
                musicList.Amount = Convert.ToInt32(amount);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Список изменен!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Удаление альбома в БД
        public void MusicListDelete(int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                MusicList musicList = context.MusicList.Where(id => id.MusicListID == choicecell).First();
                context.MusicList.Remove(musicList);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Список удален!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Добавление связи в БД
        public void RelationAdd(int musicListID, int musicID)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Relation relation = new Relation()
                {
                    MusicListID = Convert.ToInt32(musicListID),
                    MusicID = Convert.ToInt32(musicID),
                };
                context.Relation.Add(relation);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Связь добавлена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //Редактирование связи в БД
        public void RelationUpdate(int musicListID, int musicID, int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Relation relation = context.Relation.Where(id => id.RelationID == choicecell).First();
                relation.MusicListID = Convert.ToInt32(musicListID);               
                relation.MusicID = Convert.ToInt32(musicID);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Связь изменена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
        //ДУдаление связи в БД
        public void RelationDelete(int choicecell)
        {
            using (VKPlayerModelContainer context = new VKPlayerModelContainer())
            {
                Relation relation = context.Relation.Where(id => id.RelationID == choicecell).First();
                context.Relation.Remove(relation);
                try
                {
                    context.SaveChanges();
                    MessageBox.Show("Связь удалена!");
                }
                catch (Exception z)
                {
                    MessageBox.Show("Ошибка! " + z.Message);
                }
            }
        }
    }
}
