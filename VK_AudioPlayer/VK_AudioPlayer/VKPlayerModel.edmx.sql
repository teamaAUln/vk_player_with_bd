
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/07/2014 12:39:15
-- Generated from EDMX file: D:\Users\Игорь Охотников\Documents\vk_musikplayer\VK_AudioPlayer\VK_AudioPlayer\VKPlayerModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [VKPlayerDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MusicList] DROP CONSTRAINT [FK_UserList];
GO
IF OBJECT_ID(N'[dbo].[FK_RelationList_Relation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelationList] DROP CONSTRAINT [FK_RelationList_Relation];
GO
IF OBJECT_ID(N'[dbo].[FK_RelationList_List]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelationList] DROP CONSTRAINT [FK_RelationList_List];
GO
IF OBJECT_ID(N'[dbo].[FK_RelationMusic_Relation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelationMusic] DROP CONSTRAINT [FK_RelationMusic_Relation];
GO
IF OBJECT_ID(N'[dbo].[FK_RelationMusic_Music]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelationMusic] DROP CONSTRAINT [FK_RelationMusic_Music];
GO
IF OBJECT_ID(N'[dbo].[FK_LogUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Log] DROP CONSTRAINT [FK_LogUser];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Music]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Music];
GO
IF OBJECT_ID(N'[dbo].[Log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Log];
GO
IF OBJECT_ID(N'[dbo].[User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[User];
GO
IF OBJECT_ID(N'[dbo].[MusicList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MusicList];
GO
IF OBJECT_ID(N'[dbo].[Relation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Relation];
GO
IF OBJECT_ID(N'[dbo].[RelationList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelationList];
GO
IF OBJECT_ID(N'[dbo].[RelationMusic]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelationMusic];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Music'
CREATE TABLE [dbo].[Music] (
    [MusicID] int IDENTITY(1,1) NOT NULL,
    [OwnedID] int  NOT NULL,
    [Artist] nvarchar(max)  NOT NULL,
    [Title] nvarchar(max)  NULL,
    [Duration] int  NOT NULL,
    [URL] nvarchar(max)  NOT NULL,
    [LyricsID] int  NOT NULL,
    [Genre] int  NOT NULL
);
GO

-- Creating table 'Log'
CREATE TABLE [dbo].[Log] (
    [LogID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL,
    [Date] datetime  NOT NULL,
    [Note] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'User'
CREATE TABLE [dbo].[User] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [VkID] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MusicList'
CREATE TABLE [dbo].[MusicList] (
    [MusicListID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Amount] int  NOT NULL
);
GO

-- Creating table 'Relation'
CREATE TABLE [dbo].[Relation] (
    [RelationID] int IDENTITY(1,1) NOT NULL,
    [MusicListID] int  NOT NULL,
    [MusicID] int  NOT NULL
);
GO

-- Creating table 'UserList'
CREATE TABLE [dbo].[UserList] (
    [User_UserID] int  NOT NULL,
    [List_MusicListID] int  NOT NULL
);
GO

-- Creating table 'RelationList'
CREATE TABLE [dbo].[RelationList] (
    [Relation_RelationID] int  NOT NULL,
    [List_MusicListID] int  NOT NULL
);
GO

-- Creating table 'RelationMusic'
CREATE TABLE [dbo].[RelationMusic] (
    [Relation_RelationID] int  NOT NULL,
    [Music_MusicID] int  NOT NULL
);
GO

-- Creating table 'LogUser'
CREATE TABLE [dbo].[LogUser] (
    [Log_LogID] int  NOT NULL,
    [User_UserID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MusicID] in table 'Music'
ALTER TABLE [dbo].[Music]
ADD CONSTRAINT [PK_Music]
    PRIMARY KEY CLUSTERED ([MusicID] ASC);
GO

-- Creating primary key on [LogID] in table 'Log'
ALTER TABLE [dbo].[Log]
ADD CONSTRAINT [PK_Log]
    PRIMARY KEY CLUSTERED ([LogID] ASC);
GO

-- Creating primary key on [UserID] in table 'User'
ALTER TABLE [dbo].[User]
ADD CONSTRAINT [PK_User]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [MusicListID] in table 'MusicList'
ALTER TABLE [dbo].[MusicList]
ADD CONSTRAINT [PK_MusicList]
    PRIMARY KEY CLUSTERED ([MusicListID] ASC);
GO

-- Creating primary key on [RelationID] in table 'Relation'
ALTER TABLE [dbo].[Relation]
ADD CONSTRAINT [PK_Relation]
    PRIMARY KEY CLUSTERED ([RelationID] ASC);
GO

-- Creating primary key on [User_UserID], [List_MusicListID] in table 'UserList'
ALTER TABLE [dbo].[UserList]
ADD CONSTRAINT [PK_UserList]
    PRIMARY KEY CLUSTERED ([User_UserID], [List_MusicListID] ASC);
GO

-- Creating primary key on [Relation_RelationID], [List_MusicListID] in table 'RelationList'
ALTER TABLE [dbo].[RelationList]
ADD CONSTRAINT [PK_RelationList]
    PRIMARY KEY CLUSTERED ([Relation_RelationID], [List_MusicListID] ASC);
GO

-- Creating primary key on [Relation_RelationID], [Music_MusicID] in table 'RelationMusic'
ALTER TABLE [dbo].[RelationMusic]
ADD CONSTRAINT [PK_RelationMusic]
    PRIMARY KEY CLUSTERED ([Relation_RelationID], [Music_MusicID] ASC);
GO

-- Creating primary key on [Log_LogID], [User_UserID] in table 'LogUser'
ALTER TABLE [dbo].[LogUser]
ADD CONSTRAINT [PK_LogUser]
    PRIMARY KEY CLUSTERED ([Log_LogID], [User_UserID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_UserID] in table 'UserList'
ALTER TABLE [dbo].[UserList]
ADD CONSTRAINT [FK_UserList_User]
    FOREIGN KEY ([User_UserID])
    REFERENCES [dbo].[User]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [List_MusicListID] in table 'UserList'
ALTER TABLE [dbo].[UserList]
ADD CONSTRAINT [FK_UserList_List]
    FOREIGN KEY ([List_MusicListID])
    REFERENCES [dbo].[MusicList]
        ([MusicListID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserList_List'
CREATE INDEX [IX_FK_UserList_List]
ON [dbo].[UserList]
    ([List_MusicListID]);
GO

-- Creating foreign key on [Relation_RelationID] in table 'RelationList'
ALTER TABLE [dbo].[RelationList]
ADD CONSTRAINT [FK_RelationList_Relation]
    FOREIGN KEY ([Relation_RelationID])
    REFERENCES [dbo].[Relation]
        ([RelationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [List_MusicListID] in table 'RelationList'
ALTER TABLE [dbo].[RelationList]
ADD CONSTRAINT [FK_RelationList_List]
    FOREIGN KEY ([List_MusicListID])
    REFERENCES [dbo].[MusicList]
        ([MusicListID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RelationList_List'
CREATE INDEX [IX_FK_RelationList_List]
ON [dbo].[RelationList]
    ([List_MusicListID]);
GO

-- Creating foreign key on [Relation_RelationID] in table 'RelationMusic'
ALTER TABLE [dbo].[RelationMusic]
ADD CONSTRAINT [FK_RelationMusic_Relation]
    FOREIGN KEY ([Relation_RelationID])
    REFERENCES [dbo].[Relation]
        ([RelationID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Music_MusicID] in table 'RelationMusic'
ALTER TABLE [dbo].[RelationMusic]
ADD CONSTRAINT [FK_RelationMusic_Music]
    FOREIGN KEY ([Music_MusicID])
    REFERENCES [dbo].[Music]
        ([MusicID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RelationMusic_Music'
CREATE INDEX [IX_FK_RelationMusic_Music]
ON [dbo].[RelationMusic]
    ([Music_MusicID]);
GO

-- Creating foreign key on [Log_LogID] in table 'LogUser'
ALTER TABLE [dbo].[LogUser]
ADD CONSTRAINT [FK_LogUser_Log]
    FOREIGN KEY ([Log_LogID])
    REFERENCES [dbo].[Log]
        ([LogID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [User_UserID] in table 'LogUser'
ALTER TABLE [dbo].[LogUser]
ADD CONSTRAINT [FK_LogUser_User]
    FOREIGN KEY ([User_UserID])
    REFERENCES [dbo].[User]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_LogUser_User'
CREATE INDEX [IX_FK_LogUser_User]
ON [dbo].[LogUser]
    ([User_UserID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------