﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.34003
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VK_AudioPlayer {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class SettingsPanel : global::System.Configuration.ApplicationSettingsBase {
        
        private static SettingsPanel defaultInstance = ((SettingsPanel)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new SettingsPanel())));
        
        public static SettingsPanel Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool up {
            get {
                return ((bool)(this["up"]));
            }
            set {
                this["up"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int index {
            get {
                return ((int)(this["index"]));
            }
            set {
                this["index"] = value;
            }
        }
    }
}
