﻿namespace VK_AudioPlayer
{
    partial class Main
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Пустой альбом");
            this.bAuth = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.скачатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьВАльбомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bLoad = new System.Windows.Forms.Button();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.bFind = new System.Windows.Forms.Button();
            this.WMPPlay = new AxWMPLib.AxWindowsMediaPlayer();
            this.sSong = new System.Windows.Forms.SaveFileDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Tre = new System.Windows.Forms.NotifyIcon(this.components);
            this.TreMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TreSongName = new System.Windows.Forms.ToolStripTextBox();
            this.TreStart = new System.Windows.Forms.ToolStripMenuItem();
            this.TreStop = new System.Windows.Forms.ToolStripMenuItem();
            this.TreNext = new System.Windows.Forms.ToolStripMenuItem();
            this.TreBefore = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_Menu = new System.Windows.Forms.Panel();
            this.button_Help = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_Search = new System.Windows.Forms.Button();
            this.button_MyAlbum = new System.Windows.Forms.Button();
            this.button_hideMenu = new System.Windows.Forms.Button();
            this.button_MyAudio = new System.Windows.Forms.Button();
            this.timer_Album = new System.Windows.Forms.Timer(this.components);
            this.panel_Albums = new System.Windows.Forms.Panel();
            this.panel_AddNewAlbum = new System.Windows.Forms.Panel();
            this.button_OKAddNewAlbum = new System.Windows.Forms.Button();
            this.button_AddPictureNewAlbum = new System.Windows.Forms.Button();
            this.pictureBox_PictureNewAlbum = new System.Windows.Forms.PictureBox();
            this.label_NameNewAlbum = new System.Windows.Forms.Label();
            this.textBox_NameNewAlbum = new System.Windows.Forms.TextBox();
            this.listView_MyAlbum = new System.Windows.Forms.ListView();
            this.button_SearchMyAlbumHandle = new System.Windows.Forms.Button();
            this.panel_SeachMyAlbum = new System.Windows.Forms.Panel();
            this.textBox_SearchMyAlbum = new System.Windows.Forms.TextBox();
            this.button_SearchMyAlbum = new System.Windows.Forms.Button();
            this.button_AddNewAlbum = new System.Windows.Forms.Button();
            this.label_MyAlbums = new System.Windows.Forms.Label();
            this.openFileDialog_AddAlbumPicture = new System.Windows.Forms.OpenFileDialog();
            this.audioData = new System.Windows.Forms.DataGridView();
            this.timer_Frormain = new System.Windows.Forms.Timer(this.components);
            this.panel_SortOnMusic = new System.Windows.Forms.Panel();
            this.label_Genre = new System.Windows.Forms.Label();
            this.label_title = new System.Windows.Forms.Label();
            this.label_Artist = new System.Windows.Forms.Label();
            this.comboBox_SearchOnGenre = new System.Windows.Forms.ComboBox();
            this.textBox_SearchonArtist = new System.Windows.Forms.TextBox();
            this.textBox_SearchOnTitle = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.vKPlayerDBDataSet = new VK_AudioPlayer.VKPlayerDBDataSet();
            this.vKPlayerDBDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Artist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.URL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WMPPlay)).BeginInit();
            this.TreMenu.SuspendLayout();
            this.panel_Menu.SuspendLayout();
            this.panel_Albums.SuspendLayout();
            this.panel_AddNewAlbum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_PictureNewAlbum)).BeginInit();
            this.panel_SeachMyAlbum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.audioData)).BeginInit();
            this.panel_SortOnMusic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vKPlayerDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vKPlayerDBDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // bAuth
            // 
            this.bAuth.AutoSize = true;
            this.bAuth.BackColor = System.Drawing.Color.SteelBlue;
            this.bAuth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAuth.ForeColor = System.Drawing.Color.White;
            this.bAuth.Location = new System.Drawing.Point(12, 12);
            this.bAuth.Name = "bAuth";
            this.bAuth.Size = new System.Drawing.Size(85, 37);
            this.bAuth.TabIndex = 0;
            this.bAuth.Text = "Авторизация";
            this.bAuth.UseVisualStyleBackColor = false;
            this.bAuth.Click += new System.EventHandler(this.bAuth_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.скачатьToolStripMenuItem,
            this.редактироватьToolStripMenuItem,
            this.добавитьВАльбомToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 70);
            // 
            // скачатьToolStripMenuItem
            // 
            this.скачатьToolStripMenuItem.Name = "скачатьToolStripMenuItem";
            this.скачатьToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.скачатьToolStripMenuItem.Text = "Скачать";
            this.скачатьToolStripMenuItem.Click += new System.EventHandler(this.Download);
            // 
            // редактироватьToolStripMenuItem
            // 
            this.редактироватьToolStripMenuItem.Name = "редактироватьToolStripMenuItem";
            this.редактироватьToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.редактироватьToolStripMenuItem.Text = "Редактировать";
            this.редактироватьToolStripMenuItem.Click += new System.EventHandler(this.редактироватьToolStripMenuItem_Click);
            // 
            // добавитьВАльбомToolStripMenuItem
            // 
            this.добавитьВАльбомToolStripMenuItem.Name = "добавитьВАльбомToolStripMenuItem";
            this.добавитьВАльбомToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.добавитьВАльбомToolStripMenuItem.Text = "Добавить в альбом";
            // 
            // bLoad
            // 
            this.bLoad.BackColor = System.Drawing.Color.SteelBlue;
            this.bLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLoad.ForeColor = System.Drawing.Color.White;
            this.bLoad.Location = new System.Drawing.Point(103, 12);
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size(116, 37);
            this.bLoad.TabIndex = 2;
            this.bLoad.Text = "Загрузить список со страницы";
            this.bLoad.UseVisualStyleBackColor = false;
            this.bLoad.Click += new System.EventHandler(this.bLoad_Click);
            // 
            // bFind
            // 
            this.bFind.BackColor = System.Drawing.Color.SteelBlue;
            this.bFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bFind.ForeColor = System.Drawing.Color.White;
            this.bFind.Location = new System.Drawing.Point(408, 12);
            this.bFind.Name = "bFind";
            this.bFind.Size = new System.Drawing.Size(75, 37);
            this.bFind.TabIndex = 3;
            this.bFind.Text = "Поиск";
            this.bFind.UseVisualStyleBackColor = false;
            this.bFind.Click += new System.EventHandler(this.bFind_Click);
            // 
            // WMPPlay
            // 
            this.WMPPlay.Enabled = true;
            this.WMPPlay.Location = new System.Drawing.Point(12, 422);
            this.WMPPlay.Name = "WMPPlay";
            this.WMPPlay.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WMPPlay.OcxState")));
            this.WMPPlay.Size = new System.Drawing.Size(472, 43);
            this.WMPPlay.TabIndex = 4;
            this.WMPPlay.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.WMPPlay_PlayStateChange);
            // 
            // sSong
            // 
            this.sSong.Filter = "Музыка|*.mp3";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(225, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(177, 20);
            this.textBox1.TabIndex = 5;
            // 
            // Tre
            // 
            this.Tre.ContextMenuStrip = this.TreMenu;
            this.Tre.Text = "VK_PLAYER";
            this.Tre.Visible = true;
            this.Tre.Click += new System.EventHandler(this.notifyIcon1_Click);
            this.Tre.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TreClick);
            this.Tre.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.FullWindow);
            // 
            // TreMenu
            // 
            this.TreMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreSongName,
            this.TreStart,
            this.TreStop,
            this.TreNext,
            this.TreBefore});
            this.TreMenu.Name = "TreMenu";
            this.TreMenu.Size = new System.Drawing.Size(161, 117);
            this.TreMenu.Text = "VK";
            // 
            // TreSongName
            // 
            this.TreSongName.Name = "TreSongName";
            this.TreSongName.ReadOnly = true;
            this.TreSongName.Size = new System.Drawing.Size(100, 23);
            // 
            // TreStart
            // 
            this.TreStart.Name = "TreStart";
            this.TreStart.Size = new System.Drawing.Size(160, 22);
            this.TreStart.Text = "Продолжить";
            this.TreStart.Click += new System.EventHandler(this.TreStart_Click);
            // 
            // TreStop
            // 
            this.TreStop.Name = "TreStop";
            this.TreStop.Size = new System.Drawing.Size(160, 22);
            this.TreStop.Text = "Остановить";
            this.TreStop.Click += new System.EventHandler(this.TreStop_Click);
            // 
            // TreNext
            // 
            this.TreNext.Name = "TreNext";
            this.TreNext.Size = new System.Drawing.Size(160, 22);
            this.TreNext.Text = "Следующая";
            this.TreNext.Click += new System.EventHandler(this.TreNext_Click);
            // 
            // TreBefore
            // 
            this.TreBefore.Name = "TreBefore";
            this.TreBefore.Size = new System.Drawing.Size(160, 22);
            this.TreBefore.Text = "Предыдущая";
            this.TreBefore.Click += new System.EventHandler(this.TreBefore_Click);
            // 
            // panel_Menu
            // 
            this.panel_Menu.BackColor = System.Drawing.Color.SteelBlue;
            this.panel_Menu.Controls.Add(this.button_Help);
            this.panel_Menu.Controls.Add(this.button_Close);
            this.panel_Menu.Controls.Add(this.button_Search);
            this.panel_Menu.Controls.Add(this.button_MyAlbum);
            this.panel_Menu.Controls.Add(this.button_hideMenu);
            this.panel_Menu.Controls.Add(this.button_MyAudio);
            this.panel_Menu.Location = new System.Drawing.Point(1, 466);
            this.panel_Menu.Name = "panel_Menu";
            this.panel_Menu.Size = new System.Drawing.Size(200, 478);
            this.panel_Menu.TabIndex = 7;
            this.panel_Menu.DoubleClick += new System.EventHandler(this.panel_Album_DoubleClick);
            this.panel_Menu.MouseLeave += new System.EventHandler(this.panel_Album_MouseLeave);
            this.panel_Menu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_Album_MouseMove);
            // 
            // button_Help
            // 
            this.button_Help.BackColor = System.Drawing.Color.Transparent;
            this.button_Help.FlatAppearance.BorderSize = 0;
            this.button_Help.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_Help.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_Help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Help.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Help.ForeColor = System.Drawing.Color.White;
            this.button_Help.Location = new System.Drawing.Point(-8, 242);
            this.button_Help.Name = "button_Help";
            this.button_Help.Size = new System.Drawing.Size(212, 33);
            this.button_Help.TabIndex = 5;
            this.button_Help.Text = "О программе";
            this.button_Help.UseVisualStyleBackColor = false;
            // 
            // button_Close
            // 
            this.button_Close.BackColor = System.Drawing.Color.Transparent;
            this.button_Close.FlatAppearance.BorderSize = 0;
            this.button_Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Close.ForeColor = System.Drawing.Color.White;
            this.button_Close.Location = new System.Drawing.Point(-9, 293);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(212, 33);
            this.button_Close.TabIndex = 4;
            this.button_Close.Text = "Выход";
            this.button_Close.UseVisualStyleBackColor = false;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // button_Search
            // 
            this.button_Search.BackColor = System.Drawing.Color.Transparent;
            this.button_Search.FlatAppearance.BorderSize = 0;
            this.button_Search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_Search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Search.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Search.ForeColor = System.Drawing.Color.White;
            this.button_Search.Location = new System.Drawing.Point(-9, 197);
            this.button_Search.Name = "button_Search";
            this.button_Search.Size = new System.Drawing.Size(212, 33);
            this.button_Search.TabIndex = 3;
            this.button_Search.Text = "Поиск";
            this.button_Search.UseVisualStyleBackColor = false;
            // 
            // button_MyAlbum
            // 
            this.button_MyAlbum.BackColor = System.Drawing.Color.Transparent;
            this.button_MyAlbum.FlatAppearance.BorderSize = 0;
            this.button_MyAlbum.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_MyAlbum.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_MyAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_MyAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_MyAlbum.ForeColor = System.Drawing.Color.White;
            this.button_MyAlbum.Location = new System.Drawing.Point(-4, 153);
            this.button_MyAlbum.Name = "button_MyAlbum";
            this.button_MyAlbum.Size = new System.Drawing.Size(212, 33);
            this.button_MyAlbum.TabIndex = 2;
            this.button_MyAlbum.Text = "Мои альбомы";
            this.button_MyAlbum.UseVisualStyleBackColor = false;
            this.button_MyAlbum.Click += new System.EventHandler(this.button_MyAlbum_Click);
            // 
            // button_hideMenu
            // 
            this.button_hideMenu.BackColor = System.Drawing.Color.Transparent;
            this.button_hideMenu.FlatAppearance.BorderSize = 0;
            this.button_hideMenu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_hideMenu.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_hideMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_hideMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_hideMenu.Location = new System.Drawing.Point(173, 422);
            this.button_hideMenu.Name = "button_hideMenu";
            this.button_hideMenu.Size = new System.Drawing.Size(24, 53);
            this.button_hideMenu.TabIndex = 1;
            this.button_hideMenu.Text = "<";
            this.button_hideMenu.UseVisualStyleBackColor = false;
            this.button_hideMenu.Click += new System.EventHandler(this.button4_Click_2);
            // 
            // button_MyAudio
            // 
            this.button_MyAudio.BackColor = System.Drawing.Color.Transparent;
            this.button_MyAudio.FlatAppearance.BorderSize = 0;
            this.button_MyAudio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.button_MyAudio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue;
            this.button_MyAudio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_MyAudio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_MyAudio.ForeColor = System.Drawing.Color.White;
            this.button_MyAudio.Location = new System.Drawing.Point(-3, 102);
            this.button_MyAudio.Name = "button_MyAudio";
            this.button_MyAudio.Size = new System.Drawing.Size(212, 33);
            this.button_MyAudio.TabIndex = 0;
            this.button_MyAudio.Text = "Мои аудиозаписи";
            this.button_MyAudio.UseVisualStyleBackColor = false;
            this.button_MyAudio.Click += new System.EventHandler(this.button_MyAudio_Click_1);
            // 
            // timer_Album
            // 
            this.timer_Album.Interval = 1;
            this.timer_Album.Tick += new System.EventHandler(this.timer_Album_Tick);
            // 
            // panel_Albums
            // 
            this.panel_Albums.BackColor = System.Drawing.Color.White;
            this.panel_Albums.Controls.Add(this.panel_AddNewAlbum);
            this.panel_Albums.Controls.Add(this.listView_MyAlbum);
            this.panel_Albums.Controls.Add(this.button_SearchMyAlbumHandle);
            this.panel_Albums.Controls.Add(this.panel_SeachMyAlbum);
            this.panel_Albums.Controls.Add(this.button_AddNewAlbum);
            this.panel_Albums.Controls.Add(this.label_MyAlbums);
            this.panel_Albums.Location = new System.Drawing.Point(452, 463);
            this.panel_Albums.Name = "panel_Albums";
            this.panel_Albums.Size = new System.Drawing.Size(495, 478);
            this.panel_Albums.TabIndex = 8;
            this.panel_Albums.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_Albums_MouseMove);
            // 
            // panel_AddNewAlbum
            // 
            this.panel_AddNewAlbum.BackColor = System.Drawing.Color.SteelBlue;
            this.panel_AddNewAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_AddNewAlbum.Controls.Add(this.button_OKAddNewAlbum);
            this.panel_AddNewAlbum.Controls.Add(this.button_AddPictureNewAlbum);
            this.panel_AddNewAlbum.Controls.Add(this.pictureBox_PictureNewAlbum);
            this.panel_AddNewAlbum.Controls.Add(this.label_NameNewAlbum);
            this.panel_AddNewAlbum.Controls.Add(this.textBox_NameNewAlbum);
            this.panel_AddNewAlbum.Location = new System.Drawing.Point(10, 71);
            this.panel_AddNewAlbum.Name = "panel_AddNewAlbum";
            this.panel_AddNewAlbum.Size = new System.Drawing.Size(159, 354);
            this.panel_AddNewAlbum.TabIndex = 12;
            // 
            // button_OKAddNewAlbum
            // 
            this.button_OKAddNewAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_OKAddNewAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_OKAddNewAlbum.ForeColor = System.Drawing.Color.White;
            this.button_OKAddNewAlbum.Location = new System.Drawing.Point(3, 316);
            this.button_OKAddNewAlbum.Name = "button_OKAddNewAlbum";
            this.button_OKAddNewAlbum.Size = new System.Drawing.Size(150, 28);
            this.button_OKAddNewAlbum.TabIndex = 17;
            this.button_OKAddNewAlbum.Text = "Добавить";
            this.button_OKAddNewAlbum.UseVisualStyleBackColor = true;
            // 
            // button_AddPictureNewAlbum
            // 
            this.button_AddPictureNewAlbum.BackColor = System.Drawing.Color.Transparent;
            this.button_AddPictureNewAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AddPictureNewAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_AddPictureNewAlbum.ForeColor = System.Drawing.Color.White;
            this.button_AddPictureNewAlbum.Location = new System.Drawing.Point(4, 204);
            this.button_AddPictureNewAlbum.Name = "button_AddPictureNewAlbum";
            this.button_AddPictureNewAlbum.Size = new System.Drawing.Size(146, 28);
            this.button_AddPictureNewAlbum.TabIndex = 16;
            this.button_AddPictureNewAlbum.Text = "Добавить обложку";
            this.button_AddPictureNewAlbum.UseVisualStyleBackColor = false;
            this.button_AddPictureNewAlbum.Click += new System.EventHandler(this.button_AddPictureNewAlbum_Click);
            // 
            // pictureBox_PictureNewAlbum
            // 
            this.pictureBox_PictureNewAlbum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_PictureNewAlbum.Location = new System.Drawing.Point(18, 64);
            this.pictureBox_PictureNewAlbum.Name = "pictureBox_PictureNewAlbum";
            this.pictureBox_PictureNewAlbum.Size = new System.Drawing.Size(120, 120);
            this.pictureBox_PictureNewAlbum.TabIndex = 15;
            this.pictureBox_PictureNewAlbum.TabStop = false;
            // 
            // label_NameNewAlbum
            // 
            this.label_NameNewAlbum.AutoSize = true;
            this.label_NameNewAlbum.ForeColor = System.Drawing.Color.White;
            this.label_NameNewAlbum.Location = new System.Drawing.Point(8, 11);
            this.label_NameNewAlbum.Name = "label_NameNewAlbum";
            this.label_NameNewAlbum.Size = new System.Drawing.Size(142, 13);
            this.label_NameNewAlbum.TabIndex = 14;
            this.label_NameNewAlbum.Text = "Название нового альбома";
            // 
            // textBox_NameNewAlbum
            // 
            this.textBox_NameNewAlbum.Location = new System.Drawing.Point(4, 30);
            this.textBox_NameNewAlbum.Name = "textBox_NameNewAlbum";
            this.textBox_NameNewAlbum.Size = new System.Drawing.Size(152, 20);
            this.textBox_NameNewAlbum.TabIndex = 13;
            // 
            // listView_MyAlbum
            // 
            listViewItem2.StateImageIndex = 0;
            this.listView_MyAlbum.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2});
            this.listView_MyAlbum.Location = new System.Drawing.Point(10, 108);
            this.listView_MyAlbum.Name = "listView_MyAlbum";
            this.listView_MyAlbum.Size = new System.Drawing.Size(471, 314);
            this.listView_MyAlbum.TabIndex = 10;
            this.listView_MyAlbum.TileSize = new System.Drawing.Size(10, 10);
            this.listView_MyAlbum.UseCompatibleStateImageBehavior = false;
            // 
            // button_SearchMyAlbumHandle
            // 
            this.button_SearchMyAlbumHandle.BackColor = System.Drawing.Color.Transparent;
            this.button_SearchMyAlbumHandle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SearchMyAlbumHandle.Location = new System.Drawing.Point(12, 7);
            this.button_SearchMyAlbumHandle.Name = "button_SearchMyAlbumHandle";
            this.button_SearchMyAlbumHandle.Size = new System.Drawing.Size(40, 23);
            this.button_SearchMyAlbumHandle.TabIndex = 9;
            this.button_SearchMyAlbumHandle.UseVisualStyleBackColor = false;
            this.button_SearchMyAlbumHandle.Click += new System.EventHandler(this.button_SearchMyAlbumHandle_Click);
            // 
            // panel_SeachMyAlbum
            // 
            this.panel_SeachMyAlbum.BackColor = System.Drawing.Color.White;
            this.panel_SeachMyAlbum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_SeachMyAlbum.Controls.Add(this.textBox_SearchMyAlbum);
            this.panel_SeachMyAlbum.Controls.Add(this.button_SearchMyAlbum);
            this.panel_SeachMyAlbum.Location = new System.Drawing.Point(-3, 37);
            this.panel_SeachMyAlbum.Name = "panel_SeachMyAlbum";
            this.panel_SeachMyAlbum.Size = new System.Drawing.Size(500, 35);
            this.panel_SeachMyAlbum.TabIndex = 8;
            this.panel_SeachMyAlbum.Visible = false;
            // 
            // textBox_SearchMyAlbum
            // 
            this.textBox_SearchMyAlbum.Location = new System.Drawing.Point(11, 7);
            this.textBox_SearchMyAlbum.Name = "textBox_SearchMyAlbum";
            this.textBox_SearchMyAlbum.Size = new System.Drawing.Size(328, 20);
            this.textBox_SearchMyAlbum.TabIndex = 0;
            // 
            // button_SearchMyAlbum
            // 
            this.button_SearchMyAlbum.BackColor = System.Drawing.Color.SteelBlue;
            this.button_SearchMyAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SearchMyAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SearchMyAlbum.ForeColor = System.Drawing.Color.White;
            this.button_SearchMyAlbum.Location = new System.Drawing.Point(357, 4);
            this.button_SearchMyAlbum.Name = "button_SearchMyAlbum";
            this.button_SearchMyAlbum.Size = new System.Drawing.Size(117, 26);
            this.button_SearchMyAlbum.TabIndex = 1;
            this.button_SearchMyAlbum.Text = "Поиск";
            this.button_SearchMyAlbum.UseVisualStyleBackColor = false;
            // 
            // button_AddNewAlbum
            // 
            this.button_AddNewAlbum.BackColor = System.Drawing.Color.SteelBlue;
            this.button_AddNewAlbum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AddNewAlbum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_AddNewAlbum.ForeColor = System.Drawing.Color.White;
            this.button_AddNewAlbum.Location = new System.Drawing.Point(10, 431);
            this.button_AddNewAlbum.Name = "button_AddNewAlbum";
            this.button_AddNewAlbum.Size = new System.Drawing.Size(159, 28);
            this.button_AddNewAlbum.TabIndex = 12;
            this.button_AddNewAlbum.Text = "Создать новый альбом";
            this.button_AddNewAlbum.UseVisualStyleBackColor = false;
            this.button_AddNewAlbum.Click += new System.EventHandler(this.button_AddNewAlbum_Click);
            // 
            // label_MyAlbums
            // 
            this.label_MyAlbums.BackColor = System.Drawing.Color.SteelBlue;
            this.label_MyAlbums.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label_MyAlbums.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_MyAlbums.ForeColor = System.Drawing.Color.White;
            this.label_MyAlbums.Location = new System.Drawing.Point(0, 0);
            this.label_MyAlbums.Name = "label_MyAlbums";
            this.label_MyAlbums.Size = new System.Drawing.Size(495, 37);
            this.label_MyAlbums.TabIndex = 0;
            this.label_MyAlbums.Text = "Мои Альбомы";
            this.label_MyAlbums.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFileDialog_AddAlbumPicture
            // 
            this.openFileDialog_AddAlbumPicture.FileName = "openFileDialog_AddAlbumPictire";
            // 
            // audioData
            // 
            this.audioData.AllowUserToAddRows = false;
            this.audioData.AllowUserToDeleteRows = false;
            this.audioData.BackgroundColor = System.Drawing.Color.White;
            this.audioData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Artist,
            this.Title,
            this.Genre,
            this.Duration,
            this.URL,
            this.id});
            this.audioData.Location = new System.Drawing.Point(12, 103);
            this.audioData.Name = "audioData";
            this.audioData.ReadOnly = true;
            this.audioData.Size = new System.Drawing.Size(472, 313);
            this.audioData.TabIndex = 0;
            this.audioData.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.audioData_ColumnHeaderMouseClick);
            this.audioData.CurrentCellChanged += new System.EventHandler(this.audioData_CurrentCellChanged);
            this.audioData.MouseClick += new System.Windows.Forms.MouseEventHandler(this.audioData_MouseClick);
            // 
            // timer_Frormain
            // 
            this.timer_Frormain.Interval = 1;
            this.timer_Frormain.Tick += new System.EventHandler(this.timer_Frormain_Tick_1);
            // 
            // panel_SortOnMusic
            // 
            this.panel_SortOnMusic.BackColor = System.Drawing.Color.White;
            this.panel_SortOnMusic.Controls.Add(this.label_Genre);
            this.panel_SortOnMusic.Controls.Add(this.label_title);
            this.panel_SortOnMusic.Controls.Add(this.label_Artist);
            this.panel_SortOnMusic.Controls.Add(this.comboBox_SearchOnGenre);
            this.panel_SortOnMusic.Controls.Add(this.textBox_SearchonArtist);
            this.panel_SortOnMusic.Controls.Add(this.textBox_SearchOnTitle);
            this.panel_SortOnMusic.Location = new System.Drawing.Point(12, 61);
            this.panel_SortOnMusic.Name = "panel_SortOnMusic";
            this.panel_SortOnMusic.Size = new System.Drawing.Size(472, 39);
            this.panel_SortOnMusic.TabIndex = 9;
            this.panel_SortOnMusic.Visible = false;
            // 
            // label_Genre
            // 
            this.label_Genre.AutoSize = true;
            this.label_Genre.Location = new System.Drawing.Point(324, 0);
            this.label_Genre.Name = "label_Genre";
            this.label_Genre.Size = new System.Drawing.Size(36, 13);
            this.label_Genre.TabIndex = 5;
            this.label_Genre.Text = "Жанр";
            // 
            // label_title
            // 
            this.label_title.AutoSize = true;
            this.label_title.Location = new System.Drawing.Point(159, 0);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(125, 13);
            this.label_title.TabIndex = 4;
            this.label_title.Text = "Название аудиозаписи";
            // 
            // label_Artist
            // 
            this.label_Artist.AutoSize = true;
            this.label_Artist.Location = new System.Drawing.Point(22, 0);
            this.label_Artist.Name = "label_Artist";
            this.label_Artist.Size = new System.Drawing.Size(74, 13);
            this.label_Artist.TabIndex = 3;
            this.label_Artist.Text = "Исполнитель";
            // 
            // comboBox_SearchOnGenre
            // 
            this.comboBox_SearchOnGenre.FormattingEnabled = true;
            this.comboBox_SearchOnGenre.Location = new System.Drawing.Point(312, 15);
            this.comboBox_SearchOnGenre.Name = "comboBox_SearchOnGenre";
            this.comboBox_SearchOnGenre.Size = new System.Drawing.Size(121, 21);
            this.comboBox_SearchOnGenre.TabIndex = 2;
            // 
            // textBox_SearchonArtist
            // 
            this.textBox_SearchonArtist.Location = new System.Drawing.Point(15, 16);
            this.textBox_SearchonArtist.Name = "textBox_SearchonArtist";
            this.textBox_SearchonArtist.Size = new System.Drawing.Size(124, 20);
            this.textBox_SearchonArtist.TabIndex = 1;
            this.textBox_SearchonArtist.TextChanged += new System.EventHandler(this.textBox_SearchonArtist_TextChanged);
            // 
            // textBox_SearchOnTitle
            // 
            this.textBox_SearchOnTitle.Location = new System.Drawing.Point(162, 16);
            this.textBox_SearchOnTitle.Name = "textBox_SearchOnTitle";
            this.textBox_SearchOnTitle.Size = new System.Drawing.Size(124, 20);
            this.textBox_SearchOnTitle.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(279, 38);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(123, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Показать фильтры";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // vKPlayerDBDataSet
            // 
            this.vKPlayerDBDataSet.DataSetName = "VKPlayerDBDataSet";
            this.vKPlayerDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vKPlayerDBDataSetBindingSource
            // 
            this.vKPlayerDBDataSetBindingSource.DataSource = this.vKPlayerDBDataSet;
            this.vKPlayerDBDataSetBindingSource.Position = 0;
            // 
            // Artist
            // 
            this.Artist.HeaderText = "Artist";
            this.Artist.Name = "Artist";
            this.Artist.ReadOnly = true;
            // 
            // Title
            // 
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 140;
            // 
            // Genre
            // 
            this.Genre.HeaderText = "Genre";
            this.Genre.Name = "Genre";
            this.Genre.ReadOnly = true;
            // 
            // Duration
            // 
            this.Duration.HeaderText = "Duration";
            this.Duration.Name = "Duration";
            this.Duration.ReadOnly = true;
            this.Duration.Width = 60;
            // 
            // URL
            // 
            this.URL.HeaderText = "URL";
            this.URL.Name = "URL";
            this.URL.ReadOnly = true;
            this.URL.Visible = false;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 30;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(495, 477);
            this.Controls.Add(this.panel_Menu);
            this.Controls.Add(this.panel_Albums);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.panel_SortOnMusic);
            this.Controls.Add(this.audioData);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.WMPPlay);
            this.Controls.Add(this.bLoad);
            this.Controls.Add(this.bAuth);
            this.Controls.Add(this.bFind);
            this.Name = "Main";
            this.Text = "VK Audio Player";
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Click += new System.EventHandler(this.Form1_Click);
            this.MouseEnter += new System.EventHandler(this.Form1_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WMPPlay)).EndInit();
            this.TreMenu.ResumeLayout(false);
            this.TreMenu.PerformLayout();
            this.panel_Menu.ResumeLayout(false);
            this.panel_Albums.ResumeLayout(false);
            this.panel_AddNewAlbum.ResumeLayout(false);
            this.panel_AddNewAlbum.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_PictureNewAlbum)).EndInit();
            this.panel_SeachMyAlbum.ResumeLayout(false);
            this.panel_SeachMyAlbum.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.audioData)).EndInit();
            this.panel_SortOnMusic.ResumeLayout(false);
            this.panel_SortOnMusic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vKPlayerDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vKPlayerDBDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bAuth;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button bLoad;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Button bFind;
        private AxWMPLib.AxWindowsMediaPlayer WMPPlay;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem скачатьToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog sSong;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem редактироватьToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon Tre;
        private System.Windows.Forms.ContextMenuStrip TreMenu;
        private System.Windows.Forms.ToolStripMenuItem TreStop;
        private System.Windows.Forms.ToolStripMenuItem TreNext;
        private System.Windows.Forms.ToolStripMenuItem TreBefore;
        private System.Windows.Forms.ToolStripTextBox TreSongName;
        private System.Windows.Forms.ToolStripMenuItem TreStart;
        private System.Windows.Forms.ToolStripMenuItem добавитьВАльбомToolStripMenuItem;
        private System.Windows.Forms.Timer timer_Album;
        private System.Windows.Forms.Button button_MyAudio;
        private System.Windows.Forms.Button button_hideMenu;
        private System.Windows.Forms.Button button_MyAlbum;
        private System.Windows.Forms.Button button_Search;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Button button_Help;
        private System.Windows.Forms.Panel panel_Albums;
        private System.Windows.Forms.Label label_MyAlbums;
        public System.Windows.Forms.Panel panel_Menu;
        private System.Windows.Forms.Button button_SearchMyAlbumHandle;
        private System.Windows.Forms.Panel panel_SeachMyAlbum;
        private System.Windows.Forms.Button button_SearchMyAlbum;
        private System.Windows.Forms.TextBox textBox_SearchMyAlbum;
        private System.Windows.Forms.ListView listView_MyAlbum;
        private System.Windows.Forms.Panel panel_AddNewAlbum;
        private System.Windows.Forms.Button button_AddPictureNewAlbum;
        private System.Windows.Forms.PictureBox pictureBox_PictureNewAlbum;
        private System.Windows.Forms.Label label_NameNewAlbum;
        private System.Windows.Forms.DataGridView audioData;
        private System.Windows.Forms.TextBox textBox_NameNewAlbum;
        private System.Windows.Forms.Button button_AddNewAlbum;
        private System.Windows.Forms.OpenFileDialog openFileDialog_AddAlbumPicture;
        private System.Windows.Forms.Button button_OKAddNewAlbum;
        private System.Windows.Forms.Timer timer_Frormain;
        private System.Windows.Forms.Panel panel_SortOnMusic;
        private System.Windows.Forms.Label label_Genre;
        private System.Windows.Forms.Label label_title;
        private System.Windows.Forms.Label label_Artist;
        private System.Windows.Forms.ComboBox comboBox_SearchOnGenre;
        private System.Windows.Forms.TextBox textBox_SearchonArtist;
        private System.Windows.Forms.TextBox textBox_SearchOnTitle;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.BindingSource vKPlayerDBDataSetBindingSource;
        private VKPlayerDBDataSet vKPlayerDBDataSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Artist;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn URL;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        //private System.Windows.Forms.DataGridView audioData;
    }
}

