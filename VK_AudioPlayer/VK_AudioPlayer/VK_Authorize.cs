﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VK_AudioPlayer
{
    public partial class VK_Authorize : Form
    {
        public VK_Authorize()
        {
            InitializeComponent();
        }

        private void webBrowser_Auth_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            toolStripStatusLabel1.Text = "Загрузка";
        }

        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            ((WebBrowser)sender).Url = new Uri(((WebBrowser)sender).StatusText);
            e.Cancel = true;
        }

        private void VK_Authorize_Load(object sender, EventArgs e)
        {
            webBrowser_Auth.Navigate("https://oauth.vk.com/authorize?client_id=4439219&scope=audio&redirect_uri=https://oauth.vk.com/blank.html&display=popup&v=5.21&response_type=token");
        }

        private void webBrowser_Auth_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            toolStripStatusLabel1.Text = "Загруженно";
            if (webBrowser_Auth.Url.ToString().Contains('#'))
            {
                string url = webBrowser_Auth.Url.ToString();
                string l = url.Split('#')[1];
                if (l[0] == 'a')
                {
                    Settings1.Default.token = l.Split('&')[0].Split('=')[1];
                    Settings1.Default.id = l.Split('=')[3];
                    Settings1.Default.auth = true;
                    MessageBox.Show(Settings1.Default.token + " " + Settings1.Default.id);
                    this.Close();
                }
            }
        }
        
    }
}
