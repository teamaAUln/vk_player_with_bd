﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using WMPLib;
using System.Data.SqlClient;


namespace VK_AudioPlayer
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            // делаем невидимой нашу иконку в трее
            //notifyIcon1.Visible = false;                      
            Tre.Icon = new Icon("sm1.ico");
            audioData.AllowUserToAddRows = false;
        }
        List<VK_API.Audio> audioList = new List<VK_API.Audio>();
        List<VK_API.Audio> searchingOwnerAudio = new List<VK_API.Audio>();
        List<VK_API.Audio> searchingGlobalAudio = new List<VK_API.Audio>();
        int indexLoc;
        int indexAlb;
        int mouse_pointX;
        int mouse_pointY;
        int indexclic = 0;
        int indexClickAddNewAlbum =0;
        string forward = "go and";
        string forward2;
        int selectedIndex;
        //VKPlayerDBDataSet db = new VKPlayerDBDataSet();
        DBFunction db = new DBFunction();
        public enum Genres {_ = 0,Rock = 1, Pop = 2,Rap_and_HipHop = 3,Easy_Listening = 4, Dance_and_House = 5,Instrumental = 6,Metal = 7,Alternative = 21,Dubstep = 8, Jazz_and_Blues = 9,Drum_n_Bass = 10,Trance = 11,Chanson = 12,Ethnic = 13,Acoustic_and_Vocal = 14,Reggae = 15,Classical = 16,IndiePop = 17,Speech = 19,Electropop_and_Disco = 22,Other = 18 };

        private void bAuth_Click(object sender, EventArgs e)
        {
            VK_Authorize VkAuth = new VK_Authorize();
            VkAuth.ShowDialog();
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!Settings1.Default.auth)
            { Thread.Sleep(500); }

            VK_API vk_api = new VK_API();
            audioList = vk_api.AudioGet();
        }

        public int AudioListSearchAudioid(int index)
        {
            return audioList[audioData.CurrentRow.Index].aid;
        }
        public int AudioListSearchAudioGenre(int index)
        {
            return audioList[audioData.CurrentRow.Index].genre;
        }
        public string AudioListSearchAudioArtist(int index)
        {
            return audioList[audioData.CurrentRow.Index].artist;
        }
        public string AudioListSearchAudioTitle(int index)
        {
            return audioList[audioData.CurrentRow.Index].title;
        }

        private void LoadInDB(List<VK_API.Audio> a) 
        {
            for (int i = 0; i < a.Count - 1; i++)             
               db.MusicAdd(Convert.ToInt32(a[i].aid),a[i].artist, a[i].title, Convert.ToInt32(a[i].duration), a[i].url,Convert.ToInt32(a[i].lyrics_id), Convert.ToInt32(a[i].genre.ToString()));
         }

        private void LoadAudi(List<VK_API.Audio> a)
        {
            db.DeletaAllMusic();
            
            LoadInDB(a);
            //Создаем плейлист для WMP
            WMPList = WMPPlay.playlistCollection.newPlaylist("vkPL");

            //audioData.DataSource = "(localdb)\v11.0;Initial Catalog=VKPlayerDB;Integrated Security=True";
          // SqlConnection conn = new SqlConnection()
           // SqlCommand cmd = new SqlCommand("select * from Music", conn);
          //  SqlDataAdapter DA = new SqlDataAdapter(cmd);
           // DataTable qw = new DataTable();
          //  DA.Fill(qw);
          //  audioData.DataSource = qw;
            //audioData.Rows.Add(1);
            for (int i = 0; i < a.Count - 1; i++)
            {
                //Заполняем данный плейлист
                Media = WMPPlay.newMedia(a[i].url);
                WMPList.appendItem(Media);
                //db.MusicAdd(a[i].owned_id)
                Music MC = db.ExMusicbyURL(a[i].url);
                MusicList[] MCL = db.ExMusicbyList(1);
                audioData.Rows.Add(MC.Artist,MC.Title, Enum.GetName(typeof(Genres), MC.Genre),MC.Duration, MC.URL, i);
              
            
               // audioData.Rows.Add(a[i].artist, a[i].title, a[i].genre, a[i].duration, a[i].url, i.ToString());
            }
            //Передаем плейлист в WMP
            //axWindowsMediaPlayer1.currentPlaylist = WMPList;
            //axWindowsMediaPlayer1.Ctlcontrols.stop();
            WMPPlay.currentPlaylist = WMPList;
            WMPPlay.Ctlcontrols.stop();

            audioData.Refresh();

        }

        private void bLoad_Click(object sender, EventArgs e)
        {
            
            //InternetConnectionPing();
            audioData.Rows.Clear();
           // if (audioList.Count != 0)
            //{
            tmp = false;
                LoadAudi(audioList);
           // }
            //else MessageBox.Show(" Список песен пуст ");
        }

        /// <summary>
        /// Работаем с WMP
        /// </summary>

        WMPLib.IWMPPlaylist WMPList; //Создание списка песен
        WMPLib.IWMPMedia Media;
        string separator = "------------------------------------------------------------------------------------------";


        private void bFind_Click(object sender, EventArgs e)
        {
            tmp = false;
            //InternetConnectionPing();
            audioData.Rows.Clear();
            VK_API vk_api_for_searching = new VK_API();
            searchingOwnerAudio = vk_api_for_searching.AudioSearch(textBox1.Text, 1);//получение из своих

            WMPList = WMPPlay.playlistCollection.newPlaylist("SvkPL");

            bool containing_separator_in_dGrid = false;
            for (int i = 0; i < searchingOwnerAudio.Count - 1; i++)
            {
                bool containing_audio_in_list = false;

                foreach (var j in audioList)
                    if (j.aid == searchingOwnerAudio[i].aid)
                    {
                        containing_audio_in_list = true;
                        break;
                    }
                if (containing_separator_in_dGrid != true && containing_audio_in_list != true)
                {
                    audioData.Rows.Add(separator, separator, separator, separator);
                    containing_separator_in_dGrid = true;
                }
                Media = WMPPlay.newMedia(searchingOwnerAudio[i].url);
                WMPList.appendItem(Media);
                audioData.Rows.Add(searchingOwnerAudio[i].artist, searchingOwnerAudio[i].title, Enum.GetName(typeof(Genres), searchingOwnerAudio[i].genre), searchingOwnerAudio[i].duration, searchingOwnerAudio[i].url, i.ToString());
            }

            WMPPlay.currentPlaylist = WMPList;
            WMPPlay.Ctlcontrols.stop();
        }

        private void PlayWMP(object sender, MouseEventArgs e)
        {
            //Отклавливаем что клик ЛКМ и что мы выбрали не разделитель "------------"
            if (e.Button == MouseButtons.Left && audioData.CurrentRow.Cells[0].ToString() != separator)
            {
                //Пускаем на плеер выбранную песню
                WMPPlay.Ctlcontrols.play();
                WMPPlay.Ctlcontrols.currentItem = WMPPlay.currentPlaylist.get_Item(Convert.ToInt32(audioData.Rows[selectedIndex].Cells[5].Value.ToString()));
            }
        }

        bool tmp = false;
        //сортировка нажатием на шапку
        private void audioData_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            tmp = true;//счетчик на событие, если была нажата кнопка сортировки, то в маусклике на ячейку для воспроизведения песни нужно обращаться к чему то другому...
            WMPList = WMPPlay.playlistCollection.newPlaylist("fvkPL");
            for (int i = 0; i < audioData.Rows.Count - 1; i++)
            {
                if (audioData.Rows[i].Cells[0].Value.ToString() != separator)
                {//Заполняем данный плейлист
                    Media = WMPPlay.newMedia(audioData.Rows[i].Cells[4].Value.ToString());
                    WMPList.appendItem(Media);
                }
                else
                    audioData.Rows.RemoveAt(i);
            }
            //Передаем плейлист в WMP            
            WMPPlay.currentPlaylist = WMPList;
            WMPPlay.Ctlcontrols.stop();
        }

        private void audioData_MouseClick(object sender, MouseEventArgs e)
        {
            //Отклавливаем что клик ЛКМ и что мы выбрали не разделитель "------------"
            if (e.Button == MouseButtons.Left && audioList.Count != 0 && audioData.CurrentRow.Cells[0].Value.ToString() != separator)
            {
                //Пускаем на плеер выбранную песню
                WMPPlay.Ctlcontrols.play();
                //WMPPlay.Ctlcontrols.currentItem = WMPPlay.
                WMPPlay.Ctlcontrols.currentItem = WMPPlay.currentPlaylist.get_Item(selectedIndex);
            }
            if (e.Button == MouseButtons.Right && audioList.Count != 0)//&& AudioBox.SelectedItem.ToString() != separator)
            {
                contextMenuStrip1.Show(Cursor.Position);
            }

        }

        private void audioData_CurrentCellChanged(object sender, EventArgs e)
        {
            if (tmp == true)
            {
                try { selectedIndex = audioData.CurrentRow.Index; }
                catch { }
            }
            else
            {
                try { selectedIndex = Convert.ToInt32(audioData.CurrentRow.Cells[5].Value.ToString()); }
                catch { }
            }
        }

        // Делаю скачку с сохранением на диск в любое место
        private void Download(object sender, EventArgs e)
        {
            //Уснул пока вспоминал, что да как
            sSong.ShowDialog();
            WebClient webClient = new WebClient();
            webClient.DownloadFileAsync(new Uri(audioData.Rows[audioData.CurrentRow.Index].Cells[4].Value.ToString()), sSong.FileName + ".mp3");
        }

        private void WMPPlay_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (WMPPlay.playState == WMPLib.WMPPlayState.wmppsMediaEnded)
            {
                if (audioData.CurrentRow.Index < audioList.Count - 1)
                {
                    if (audioData.Rows[audioData.CurrentRow.Index + 1].Cells[0].Value.ToString() == separator)
                        selectedIndex += 2;
                    else
                        selectedIndex++;
                    audioData.CurrentCell = audioData.Rows[selectedIndex].Cells[0];
                }
            }         
        }

        private void редактироватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditMusic edit = new EditMusic();
            edit.fm1 = this;
            edit.ShowDialog();
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                Tre.Visible = true;
            }
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            
            /*if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }*/
        }

        private void notifyClick(object sender, EventArgs e)
        {
            MessageBox.Show("sdf");
        }

        private void TreClick(object sender, MouseEventArgs e)
        {
            
        }

        private void TreStop_Click(object sender, EventArgs e)
        {
            WMPPlay.Ctlcontrols.pause();
        }

        private void TreNext_Click(object sender, EventArgs e)
        {
            WMPPlay.Ctlcontrols.next();
        }

        private void TreBefore_Click(object sender, EventArgs e)
        {
            WMPPlay.Ctlcontrols.previous();
        }

        private void TreStart_Click(object sender, EventArgs e)
        {
            WMPPlay.Ctlcontrols.play();
        }

        private void FullWindow(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                Tre.Visible = false;
                this.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //timer_Album.Enabled = true;
        }

        private void timer_Album_Tick(object sender, EventArgs e)
        {
            if (forward == "go and")
            {
                indexLoc += 10;
                if (indexLoc == 200) { timer_Album.Enabled = false;}
                if (indexLoc <= 200)
                {
                    panel_Menu.Location = new Point(panel_Menu.Location.X + 10, 0);
                }
            }

           
            if (forward == "go")
            {
                indexLoc -= 10;
                if (indexLoc == -200) { timer_Album.Enabled = false; forward = "go and"; }
                if (indexLoc >= -200)
                {
                    panel_Menu.Location = new Point(panel_Menu.Location.X - 10, 0);
                }
            }

            if (forward2 == "go and Albums")
            {
                indexAlb += 55;
                if (indexAlb == 495) { timer_Album.Enabled = false; }
                if (indexAlb <= 495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X + 55, 0);
                }
               
             }

            if (panel_Albums.Location.X > -100)
            {

                if (forward == "go and Alb")
                {
                    if (panel_Menu.Location.X != -200)
                    {
                        timer_Album.Enabled = true;
                        indexLoc -= 10;
                        if (indexLoc == -200) { forward = "go and"; timer_Album.Enabled = false; }
                        if (indexLoc >= -200)
                        {
                            panel_Menu.Location = new Point(panel_Menu.Location.X - 10, 0);
                        }
                    }
                }
            }
            


            if (forward2 == "go Albums")
            {
                indexAlb -= 55;
                if (indexAlb == -495) { timer_Album.Enabled = false; forward2 = "go and Albums"; }
                if (indexAlb >= -495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X - 55, 0);
                }
            }

            if (SettingsPanel.Default.up == true)
            {
                SettingsPanel.Default.index++;
                if (SettingsPanel.Default.index <= 14)
                {
                    panel_AddNewAlbum.Height += 28;
                    if (SettingsPanel.Default.index > 1)
                    {
                        panel_AddNewAlbum.Location = new Point(panel_AddNewAlbum.Location.X,panel_AddNewAlbum.Location.Y-28);
                    }
                }

            }

            //if (SettingsPanel.Default.up == "on")
            //{
            //    SettingsPanel.Default.index++;
            //    if (SettingsPanel.Default.index == 20) { timer_Album.Enabled = false; SettingsPanel.Default.up = "off"; }
            //    if (SettingsPanel.Default.index <= 20)
            //    {
            //        panel_SeachMyAlbum.Width += 25;

            //        if (SettingsPanel.Default.index > 12)
            //        {
            //            panel_SeachMyAlbum.Height += 5;
            //        }
            //    }

            //    if (SettingsPanel.Default.up == "off")
            //    {
            //        SettingsPanel.Default.index++;
            //        if (SettingsPanel.Default.index == 20) { timer_Album.Enabled = false; SettingsPanel.Default.up = "on"; }
            //        if (SettingsPanel.Default.index <= 20)
            //        {
            //            panel_SeachMyAlbum.Width -= 25;

            //            if (SettingsPanel.Default.index > 12)
            //            {
            //                panel_SeachMyAlbum.Height -= 5;
            //            }
            //        }
            //    }

            //}

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           //Form1 fm1 = new Form1();
           //fm1.Controls.Add(panel_Menu);
           panel_Menu.Location = new Point(-200,0);
           panel_Menu.Show();
           panel_Albums.Location = new Point(-495,0);
           //panel_SeachMyAlbum.Height = 0;
           //panel_SeachMyAlbum.Width = 0;
           panel_Albums.Controls.Add(panel_SeachMyAlbum);
           panel_Albums.Controls.Add(label_MyAlbums);
           panel_AddNewAlbum.Visible = false;

           
           //db.MusicAdd(123, "123", "123", 123, "123", 123, 123);

           

        }

        private void textBox_SearchonArtist_TextChanged(object sender, EventArgs e)
        {
            //audioData.DataSource = 
            string field = audioData.Columns[audioData.SelectedCells[0].ColumnIndex].Name;
            BindingSource bSource = (audioData.DataSource as BindingSource);
            DataTable dTable = (bSource.DataSource as DataTable);
            DataRow[] rows = dTable.Select(String.Format(field + " like '%{0}%'", textBox_SearchonArtist.Text));
            if (rows != null)
            {
                int idx = dTable.Rows.IndexOf(rows[0]);
                bSource.Position = bSource.Find(field, rows[0][field].ToString());
            }
//            string col_name;
//            string value;
//            BindingSource BSource = new System.Windows.Forms.BindingSource();
//            DataTable dt = new DataTable();
//            //BSource.DataSource = dt;
//            audioData.DataSource = BSource;
//            BSource.
//            DataRow[] dr = new DataRow[0];
///*            if (value.Length > 0)
//                dr = dt.Select("Convert(" + col_name + ", 'System.String')" + " LIKE '" + value + "*'");

//            try
//            {
//                BSource.Position = BSource.Find(field, dr[0][col_name].ToString());
//            }
//            catch (Exception ex)
//            {
//                //
//            }
//            */

//            BSource.Filter = string.Format("Artist LIKE '%{0}%'", textBox_SearchonArtist.Text);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            timer_Album.Enabled = true;
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            //if (e.X > 0 && e.X < 10)
            //{
            //    timer_Album.Enabled = true;
            //}
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X > 0 && e.X < 10)
            {
                forward = "go and";
                timer_Album.Enabled = true;
                //indexLoc = 0;
            }
        }

        private void panel_Album_MouseMove(object sender, MouseEventArgs e)
        {
            mouse_pointX = e.X;
            mouse_pointY = e.Y;
        }

        private void panel_Album_DoubleClick(object sender, EventArgs e)
        {
            if (mouse_pointY > 450 || mouse_pointY < 20 && mouse_pointX > 190)
            {
                timer_Album.Enabled = true;
                forward = "go";
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            //if (forward == "go end")
            //{
            //    timer_Album.Enabled = true;
            //}
        }

        private void panel_Album_MouseLeave(object sender, EventArgs e)
        {
            ////MessageBox.Show("УУУУУУУУУУУУ");
            //if (mouse_pointX > 200)
            //{
            //    forward = "go";
            //    timer_Album.Enabled = true;
            //}
          
        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            forward = "go";
            timer_Album.Enabled = true;
        }

        private void button_MyAlbum_Click(object sender, EventArgs e)
        {
            forward2 = "go and Albums";
            timer_Album.Enabled = true;
            forward = "go and Alb";
            indexAlb = 0;
            //ImageList imageList1= new ImageList();
            //imageList1.Images.Add("first", Image.FromFile("PictureAlbums\\BestHDWallpapersPack1121_31.jpg"));
            //listView_MyAlbum.Items.Add("One", "Пустой альбом", "first");
            AlbumsFill();
        }

        private void panel_Albums_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X > 0 && e.X < 30)
            {
                forward = "go and";
                timer_Album.Enabled = true;
               // indexLoc = 0;

                //MessageBox.Show(panel_Menu.Location.X.ToString());

                //for (int i = 0; i < 200; i += 10)
                //{
                //    panel_Menu.Location = new Point(panel_Menu.Location.X + 10, 0);
                //}
            }
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_SearchMyAlbumHandle_Click(object sender, EventArgs e)
        {
            indexclic++;
            if (indexclic % 2 == 1)
            {
                panel_SeachMyAlbum.Visible = true;
            }
            else
            {
                panel_SeachMyAlbum.Visible = false;
            }
        }

        public void AlbumsFill()
        {
            listView_MyAlbum.Clear();

            //ListView listView1 = new ListView();
            //listView1.LabelEdit = true;

            //listView_MyAlbum.Sorting = SortOrder.Ascending;

            ListViewItem item1 = new ListViewItem("Nikitka", 0);
            ListViewItem item2 = new ListViewItem("Best", 1);
            ListViewItem item3 = new ListViewItem("Friend", 2);
            listView_MyAlbum.Columns.Add("Main!", -2, HorizontalAlignment.Left);
            listView_MyAlbum.Items.AddRange(new ListViewItem[] { item1, item2, item3 });
            ImageList imageListSmall = new ImageList();
            ImageList imageListLarge = new ImageList();
            imageListLarge.ImageSize = new Size(110, 110);


            imageListSmall.Images.Add(Bitmap.FromFile("PictureAlbums\\1.jpg"));
            //imageListSmall.Images.Add(Bitmap.FromFile("C:/Users/Иван/Desktop/z_c3239d02.jpg"
            //));
            //imageListLarge.Images.Add(Bitmap.FromFile("C:/Users/Иван/Desktop/z_c3239d02.jpg"
            //));
            imageListLarge.Images.Add(Bitmap.FromFile("PictureAlbums\\1.jpg"));
            imageListLarge.Images.Add(Bitmap.FromFile("PictureAlbums\\2.jpg"));
            imageListLarge.Images.Add(Bitmap.FromFile("PictureAlbums\\3.jpg"));
            listView_MyAlbum.LargeImageList = imageListLarge;

            //listView_MyAlbum.SmallImageList = imageListSmall;


            //this.Controls.Add(listView1);
        
        }

        private void button_AddNewAlbum_Click(object sender, EventArgs e)
        {
            indexClickAddNewAlbum++;
            if (indexClickAddNewAlbum % 2 == 1)
            {
                panel_AddNewAlbum.Visible = true;
            }
            else {
                panel_AddNewAlbum.Visible = false;
            }
        }

        private void button_AddPictureNewAlbum_Click(object sender, EventArgs e)
        {
            openFileDialog_AddAlbumPicture.ShowDialog();
            pictureBox_PictureNewAlbum.BackgroundImage = Image.FromFile(openFileDialog_AddAlbumPicture.FileName);
        }


        private void button_MyAudio_Click(object sender, EventArgs e)
        {
            if (panel_Albums.Location.X > -100)
            {
                forward2 = "go Albums Main";
                timer_Frormain.Enabled = true;
                indexAlb = 0;
            }
            //else if (panel_Albums.Location.X<0)
            //{
            // forward2 = "go and Albums Main";
            // timer_Frormain.Enabled = true;
            // indexAlb = 0;
            //}
        }

        private void timer_Frormain_Tick(object sender, EventArgs e)
        {
            if (forward2 == "go and Albums Main")
            {
                indexAlb += 55;
                if (indexAlb == 495) { timer_Frormain.Enabled = false; }
                if (indexAlb <= 495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X + 55, 0);
                }

            }

            if (forward2 == "go Albums Main")
            {
                indexAlb -= 55;
                if (indexAlb == -495) { timer_Frormain.Enabled = false; forward2 = "go and Albums main"; }
                if (indexAlb >= -495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X - 55, 0);
                }
            }

        }

        private void button_MyAudio_Click_1(object sender, EventArgs e)
        {
            if (panel_Albums.Location.X > -100)
            {
                forward2 = "go Albums Main";
                timer_Frormain.Enabled = true;
                indexAlb = 0;
            }
        }

        private void timer_Frormain_Tick_1(object sender, EventArgs e)
        {
            if (forward2 == "go and Albums Main")
            {
                indexAlb += 55;
                if (indexAlb == 495) { timer_Frormain.Enabled = false; }
                if (indexAlb <= 495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X + 55, 0);
                }

            }

            if (forward2 == "go Albums Main")
            {
                indexAlb -= 55;
                if (indexAlb == -495) { timer_Frormain.Enabled = false; forward2 = "go and Albums main"; }
                if (indexAlb >= -495)
                {
                    panel_Albums.Location = new Point(panel_Albums.Location.X - 55, 0);
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (panel_SortOnMusic.Visible == true)
                panel_SortOnMusic.Visible = false;
            else
                panel_SortOnMusic.Visible = true;
        }

        


        //private void button4_Click_3(object sender, EventArgs e)
        //{
        //    forward = "go end";
        //    timer_Album.Enabled = true;
        //}

    }
}
